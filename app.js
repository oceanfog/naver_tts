/*eslint-env node*/

//------------------------------------------------------------------------------
// node.js starter application for Bluemix
//------------------------------------------------------------------------------

// This application uses express as its web server
// for more info, see: http://expressjs.com
var express = require('express');

// cfenv provides access to your Cloud Foundry environment
// for more info, see: https://www.npmjs.com/package/cfenv
var cfenv = require('cfenv');

// create a new express server
var app = express();

// serve the files out of ./public as our main files
app.use(express.static(__dirname + '/public'));

// get the app environment from Cloud Foundry
var appEnv = cfenv.getAppEnv();

var client_id = 'VDbFy6nceGqeVmJ5DHmH';
var client_secret = '4tfKeHeujq';
var fs = require('fs');
app.get('/tts', function (req, res) {

  //query
   var target_text = req.query.target_text;
   var speaker = req.query.speaker;

   var api_url = 'https://openapi.naver.com/v1/voice/tts.bin';
   var request = require('request');
   var options = {
       url: api_url,
       form: {'speaker':speaker, 'speed':'0', 'text':target_text},
       headers: {'X-Naver-Client-Id':client_id, 'X-Naver-Client-Secret': client_secret}
    };

    var writeStream = fs.createWriteStream('./tts1.mp3');
    var _req = request.post(options).on('response', function(response) {
       //console.log(response.statusCode) // 200
       //console.log(response.headers['content-type'])
    });
  _req.pipe(writeStream); // print to file
  _req.pipe(res); // print to browser
 });

 // start server on the specified port and binding host
 app.listen(appEnv.port, '0.0.0.0', function() {
   // print a message when the server starts listening
   console.log(appEnv.url + '/tts app listening!');
 });
